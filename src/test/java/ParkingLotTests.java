import org.junit.Assert;
import org.junit.Test;
import services.ParkingLotService;

/**
 * Created by supreeth.vp on 12/01/17.
 */
public class ParkingLotTests {

    @Test
    public void shouldCreateFreeSlots(){
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("6");
        Assert.assertEquals(6,parkingLotService.getParkingLot().getFreeSlots().size());
    }

    @Test
    public void shouldAllocateFreeSlotForCarWhenSlotsAreFree(){
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("6");
        parkingLotService.park("abc","white");
        Assert.assertEquals(5,parkingLotService.getParkingLot().getFreeSlots().size());
    }

    @Test
    public void shouldAlertWhenTryingToParkWhenSlotsAreFull() {
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("1");
        parkingLotService.park("abc","white");
        parkingLotService.park("def","white");
        Assert.assertEquals(0,parkingLotService.getParkingLot().getFreeSlots().size());
        Assert.assertEquals(1,parkingLotService.getParkingLot().getOccupiedSlots().size());
    }

    @Test
    public void shouldFreeUpTheSlotWhenTheCarInAParticularSlotLeaves()
    {
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("1");
        parkingLotService.park("abc","white");
        Assert.assertEquals(0,parkingLotService.getParkingLot().getFreeSlots().size());
        parkingLotService.leave("1");
        Assert.assertEquals(1,parkingLotService.getParkingLot().getFreeSlots().size());
    }

    @Test
    public void shouldReturnAllRegistrationNumberOfTheCarsParkedWithColorWhite(){
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("2");
        parkingLotService.park("abc","white");
        parkingLotService.park("def","black");
        Assert.assertEquals(1,parkingLotService.getParkingLot().getRegistrationNumbersForColor("white").size());
    }

    @Test
    public void shouldReturnAllCarsInParkingLot() {
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("2");
        parkingLotService.park("abc","white");
        parkingLotService.park("def","white");
        Assert.assertEquals(2,parkingLotService.getParkingLot().getOccupiedSlots().size());
    }

    @Test
    public void shouldReturnSlotNumberForGivenRegistrationNumber() {
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("2");
        parkingLotService.park("abc","white");
        parkingLotService.park("def","black");
        Assert.assertEquals(1,parkingLotService.getParkingLot().getSlotIdForRegistrationNumber("abc").intValue());
    }

    @Test
    public void shouldReturnRegistrationNumbersOfParticularColor() {
        ParkingLotService parkingLotService = new ParkingLotService();
        parkingLotService.createParkingLot("2");
        parkingLotService.park("abc","white");
        parkingLotService.park("def","black");
        Assert.assertEquals(1,parkingLotService.getParkingLot().getRegistrationNumbersForColor("black").size());
    }

}
