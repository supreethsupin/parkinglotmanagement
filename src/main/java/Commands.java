import services.ParkingLotService;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by supreeth.vp on 13/01/17.
 */
public class Commands {
    public Map<String, Method> commandsMap;

    public Commands() {
        commandsMap = new HashMap<String, Method>();
        try {
            populateCommandsHashMap();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    private void populateCommandsHashMap() throws NoSuchMethodException {
        commandsMap.put("create_parking_lot", ParkingLotService.class.getMethod("createParkingLot", String.class));
        commandsMap.put("park", ParkingLotService.class.getMethod("park", String.class, String.class));
        commandsMap.put("leave", ParkingLotService.class.getMethod("leave", String.class));
        commandsMap.put("status", ParkingLotService.class.getMethod("status"));
        commandsMap.put("registration_numbers_for_cars_with_colour", ParkingLotService.class.getMethod("getRegistrationNumbersFromColor", String.class));
        commandsMap.put("slot_numbers_for_cars_with_colour", ParkingLotService.class.getMethod("getSlotNumbersFromColor", String.class));
        commandsMap.put("slot_number_for_registration_number", ParkingLotService.class.getMethod("getSlotNumberFromRegistrationNumber", String.class));
    }
}
