import services.ParkingLotService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by supreeth.vp on 13/01/17.
 */
public class ParkingLotApplication {
    public static void main(String[] args) {

        InputParser inputParser = new InputParser(new ParkingLotService());
        switch (args.length) {
            case 0:
                // Interactive command-line input/output, Run an infinite loop
                for (;;) {
                    try {
                        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                        String inputString = bufferRead.readLine();
                        if (inputString.equalsIgnoreCase("exit")) {
                            break;
                        } else if ((inputString == null) || (inputString.isEmpty())) {

                        } else {
                            inputParser.parseTextInput(inputString.trim());
                        }
                    } catch(IOException e) {
                        System.out.println("Error in reading the input from console.");
                        e.printStackTrace();
                    }
                }
                break;
            case 1:
                // File input/output
                inputParser.parseFileInput(args[0]);
                break;
            default:
                System.out.println("Incorrect command usage. Usage: java -jar <jar_file_path> <input_file_path>");
        }
    }
}
