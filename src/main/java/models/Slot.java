package models;

/**
 * Created by supreeth.vp on 12/01/17.
 */
public class Slot {
    int id;
    int distanceFromEntrace;
    Car car;
    public Slot(int id,int distanceFromEntrace)
    {
        this.id = id;
        this.distanceFromEntrace= distanceFromEntrace;
    }

    public void setCar(Car car)
    {this.car = car;}

    public int getDistanceFromEntrace(){
        return this.distanceFromEntrace;
    }

    public int getId(){return this.id;}

    public Car getCar(){return this.car;}
}
