package models;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by supreeth.vp on 13/01/17.
 */
public class ParkingLot {

    SortedSet<Slot> freeSlots;
    Map<Integer, Slot> slotIdToOccupiedSlotMap;

    //reverseIndexed for the query pattern. so this can be easily moved to the db when we need more scale
    Map<String, List<String>> colorRegistrationNumberMap;
    Map<String, Integer> registrationNumberSlotIdMap;
    Map<String, List<Integer>> colorSlotIdsMap ;

    public ParkingLot(){
        freeSlots = new TreeSet(new Comparator<Slot>() {
            public int compare(Slot o1, Slot o2) {
                return o1.getDistanceFromEntrace() - o2.getDistanceFromEntrace();
            }
        });
        slotIdToOccupiedSlotMap = new ConcurrentHashMap<>();
        colorRegistrationNumberMap = new ConcurrentHashMap();
        registrationNumberSlotIdMap = new ConcurrentHashMap();
        colorSlotIdsMap = new ConcurrentHashMap();
        colorSlotIdsMap = new ConcurrentHashMap();
    }
    public SortedSet<Slot> getFreeSlots() {
        return freeSlots;
    }

    public void addSlotIdToSlotMap(Integer id, Slot slot) {
        slotIdToOccupiedSlotMap.put(id, slot);
    }

    public Slot removeSlotIdToSlotMap(Integer id) {
        return slotIdToOccupiedSlotMap.remove(id);
    }

    public List<Slot> getOccupiedSlots(){
        return new ArrayList<>(slotIdToOccupiedSlotMap.values());
    }
    public void addToRegistrationNumberSlotIdMap(String registrationNumber, int slotId) {
        registrationNumberSlotIdMap.put(registrationNumber, slotId);
    }

    public Integer removeRegistrationNumberSlotIdMap(String registrationNumber) {
        return registrationNumberSlotIdMap.remove(registrationNumber);
    }

    public void addToColorRegistrationNumberMap(String color, String registrationNumber) {
        if (colorRegistrationNumberMap.get(color) != null)
            colorRegistrationNumberMap.get(color).add(registrationNumber);
        else {
            List<String> registrationNumbers = new ArrayList();
            registrationNumbers.add(registrationNumber);
            colorRegistrationNumberMap.put(color, registrationNumbers);
        }
    }

    public List<String> getRegistrationNumbersForColor(String color) {
        return colorRegistrationNumberMap.get(color);
    }

    public void removeColorRegistrationNumberMapping(String color, String registrationNumber) {
        colorRegistrationNumberMap.get(color).remove(registrationNumber);
    }

    public Integer getSlotIdForRegistrationNumber(String registrationNumber) {
        return registrationNumberSlotIdMap.get(registrationNumber);

    }


    public void addToColorSlotIdsMap(String color, Integer slotId) {
        if (colorSlotIdsMap.get(color) != null)
            colorSlotIdsMap.get(color).add(slotId);
        else {
            List<Integer> slotIds = new ArrayList();
            slotIds.add(slotId);
            colorSlotIdsMap.put(color, slotIds);
        }
    }

    public void removeSlotIdFromSlotIdsForColorMap(String color,Integer slotId)
    {
        colorSlotIdsMap.get(color).remove(slotId);
    }
    public List<String> getSlotIdsForColor(String color){
        return colorSlotIdsMap.get(color).stream().map( id -> String.valueOf(id)).collect(Collectors.toList());
    }
}
