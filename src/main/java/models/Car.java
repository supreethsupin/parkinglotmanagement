package models;

/**
 * Created by supreeth.vp on 12/01/17.
 */
public class Car {
    java.lang.String registrationNumber;
    String color;

    public Car(String registrationNumber, String color){
        this.registrationNumber = registrationNumber;
        this.color = color;
    }

    void setColor(String color){
        this.color = color;
    }
    void setRegistrationNumber(java.lang.String registrationNumber)
    {
        this.registrationNumber = registrationNumber;
    }

    public String getColor()
    {
        return this.color;
    }

    public String getRegistrationNumber()
    {
        return this.registrationNumber;
    }
}
