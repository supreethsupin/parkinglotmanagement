package services;

import models.Car;
import models.ParkingLot;
import models.Slot;

import java.util.NoSuchElementException;

/**
 * Created by supreeth.vp on 13/01/17.
 */

public  class SlotManagementService {

    // allots slots in a given parking lot for the given car

    public synchronized void allotSlot(ParkingLot parkingLot,Car car){
        Slot allocatedSlot = null;
        try {
            allocatedSlot = parkingLot.getFreeSlots().first();
            parkingLot.getFreeSlots().remove(allocatedSlot);
        }
        catch(NoSuchElementException e)
        {
            System.out.println("Sorry, parking lot is full");
            return;
        }
        allocatedSlot.setCar(car);
        int slotId = allocatedSlot.getId();
        parkingLot.addSlotIdToSlotMap(slotId,allocatedSlot);
        parkingLot.addToColorSlotIdsMap(car.getColor(),slotId);
        parkingLot.addToRegistrationNumberSlotIdMap(car.getRegistrationNumber(),slotId);
        parkingLot.addToColorRegistrationNumberMap(car.getColor(),car.getRegistrationNumber());
        System.out.println("Allocated slot number: "+slotId);
    }


    // frees up the slot for the given slotId in the given parkingLot
    public synchronized void freeSlot(ParkingLot parkingLot,int id)
    {
        Slot freedSlot = parkingLot.removeSlotIdToSlotMap(id);
        Car car = freedSlot.getCar();
        parkingLot.removeSlotIdToSlotMap(freedSlot.getId());
        parkingLot.removeSlotIdFromSlotIdsForColorMap(car.getColor(),id);
        parkingLot.removeRegistrationNumberSlotIdMap(car.getRegistrationNumber());
        parkingLot.removeColorRegistrationNumberMapping(car.getColor(),car.getRegistrationNumber());
        freedSlot.setCar(null);
        parkingLot.getFreeSlots().add(freedSlot);
        System.out.println("Slot number " + id + " is free");
    }

    public int getSlotIdForRegistrationNumber(ParkingLot parkingLot,String registrationNumber)
    {
        return parkingLot.getSlotIdForRegistrationNumber(registrationNumber);
    }
}
