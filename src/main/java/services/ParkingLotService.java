package services;

import models.Car;
import models.ParkingLot;
import models.Slot;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created by supreeth.vp on 13/01/17.
 */
public class ParkingLotService {
    ParkingLot parkingLot;
    SlotManagementService slotManagementService=new SlotManagementService();

    public ParkingLot getParkingLot()
    {
        return this.parkingLot;
    }
    public void createParkingLot(String size)
    {
        this.parkingLot = new ParkingLot();
        for(Integer i =0;i<Integer.parseInt(size);i++)
            parkingLot.getFreeSlots().add(new Slot(i+1,i*5));

        System.out.println("Created a parking lot with " +size +" slots");
    }

    public void park(String registrationNumber,String color)
    {
        Car carToBeParked = new Car(registrationNumber, color);
        slotManagementService.allotSlot(parkingLot,carToBeParked);

    }

    public void leave(String id)
    {
        slotManagementService.freeSlot(parkingLot,Integer.parseInt(id));
    }

    public void getRegistrationNumbersFromColor(String color){
        List<String> registrationNumbers = parkingLot.getRegistrationNumbersForColor(color);
        System.out.println(String.join(",",registrationNumbers));
    }


    public void status()
    {
        List<Slot> occupiedSlots = parkingLot.getOccupiedSlots();
        Collections.sort(occupiedSlots, (o1, o2) -> o1.getDistanceFromEntrace() - o2.getDistanceFromEntrace());
        for(Slot slot:occupiedSlots){
            System.out.println(slot.getId() + "\t" + slot.getCar().getRegistrationNumber() + "\t" + slot.getCar().getColor());
        }
    }


    public void getSlotNumberFromRegistrationNumber(String registrationNumber){
        Integer slotId = parkingLot.getSlotIdForRegistrationNumber(registrationNumber);
        if(Objects.nonNull(slotId))
            System.out.println(String.valueOf(slotId));
        else
            System.out.println("Not found");
        System.out.println();
    }

    public void getSlotNumbersFromColor(String color)
    {
        System.out.println(String.join(",",parkingLot.getSlotIdsForColor(color)));
    }
}
